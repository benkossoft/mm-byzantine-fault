# mm-byzantine-fault

MM-Byzantine-Fault simulates distributed system dealing with Byzantine Fault. It implements Byzantine Fault Tolerant
Consensus for Proof-of-Stake in Blockchain applications.

**Implemented by:**

 * [M.Mucahid Benlioglu](https://github.com/mbenlioglu)
 * [Mert Kosan](https://github.com/mertkosan)


## Getting Started

**Prerequisites:**

- [Python](https://docs.python.org/2/) (developed and tested in python 2.7)
- [pip](https://pip.pypa.io/en/stable/) for python package management

### Installation and Running

Firstly, clone or download this repository, then run the following command in the project root to install needed
dependencies
    
    $ pip install -r requirements.txt
    
After installation succeeds, you can start the distributed system by giving two parameters. First paramater is number
processes, second is number of traitor nodes in the system by the following command

    $ python pbft.py -p <number_of_processes> -t <number_of_traitors>

Program prints important information during the process. However you can specify a flag (--verbose) to print every
information to command line

    $ python pbft.py -p <number_of_processes> -t <number_of_traitors> --verbose

The default proposer of the system is traitor if traitor is available in the system. However, you can add flag
(--honest_proposer) to make honest process proposer by the following flag
    
    $ python pbft.py --honest_proposer -p <number_of_processes> -t <number_of_traitors>
