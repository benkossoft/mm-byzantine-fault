import random
import sys
import hashlib

if sys.version_info < (3, 6):
    import sha3


def generate_transaction():
    return ('%064x' % random.randrange(16 ** 58)).encode('utf-8')


def generate_block(size=10):
    return '\n'.join([generate_transaction() for _ in range(size)])


def increment_dict(dictionary, field):
    if dictionary.get(field):
        dictionary[field] += 1
    else:
        dictionary[field] = 1


def hash_check(m, h):
    return hashlib.sha3_256(m.encode('utf-8')).hexdigest() == h


def hash_value(m):
    return hashlib.sha3_256(m.encode('utf-8')).hexdigest()
