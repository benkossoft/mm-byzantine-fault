import sys
import zmq
import argparse
import random
import json
import time
from multiprocessing import Process, Lock, freeze_support
from Queue import Queue
from threading import Thread
from util import generate_block, increment_dict, hash_check, hash_value


class Node:
    def __init__(self, **kwargs):
        self._io_lock = kwargs.get('io_lock')
        self._verbose = kwargs.get('verbose')
        self._id = kwargs.get('id')
        self._is_proposer = kwargs.get('is_proposer')
        self._addrs = kwargs.get('addrs')
        self._h = kwargs.get('h')

        self._proposer_id = kwargs.get('proposer_id')
        self._round = 1

        self._msg = None
        self._block = None
        self._msg_queue = Queue()

        self._recv_thread = Thread(target=self._recv, name="Receiver Thread")
        self._send_thread = Thread(target=self._send, name="Sender Thread")

        self.__context = zmq.Context()
        self._socket_send = self.__context.socket(zmq.PUSH)
        self._socket_recv = self.__context.socket(zmq.PULL)

    def start(self):
        if self._is_proposer:
            time.sleep(1)
        self._recv_thread.start()
        self._send_thread.start()

    def join(self):
        self._recv_thread.join()
        self._send_thread.join()

    def _print(self, *args, **kwargs):
        if self._verbose or kwargs.get('ignore_verbose'):
            with self._io_lock:
                sys.stdout.write(' '.join(args) + '\n')
                sys.stdout.flush()

    def _recv(self):
        self._socket_recv.bind(self._addrs[self._id])

    def _send(self):
        while True:
            msg = self._msg_queue.get()
            if len(msg['targets']) >= 1:
                self._print("Node %s is sending %s message: %s to %s"
                            % (self._id, msg['payload']['type'], msg['payload']['msg'], msg['targets']))
                for t in msg['targets']:
                    self._socket_send.connect(self._addrs[t])
                    self._socket_send.send(json.dumps(msg['payload']))
                    self._socket_send.disconnect(self._addrs[t])

    def _init_proposer(self):
        self._print("\n======================", ignore_verbose=True)
        self._print("ROUND %d is starting!" % self._round, ignore_verbose=True)
        self._print("Current blockchain: %s" % str(self._h), ignore_verbose=True)
        self._print("======================\n", ignore_verbose=True)
        self._msg = generate_block()
        self._next_round_count = 0

    def _reset_round(self):
        self._msg = None
        self._block = None

    def _put_queue(self, _type, _message, _targets):
        self._msg_queue.put({
            'payload': {
                'type': _type,
                'msg': _message,
                'id': self._id,
                'round': self._round
            },
            'targets': _targets
        })
        # For simulation
        time.sleep(random.uniform(0.5, 1.5))


class Honest(Node):
    def __init__(self, **kwargs):
        Node.__init__(self, **kwargs)

        self._k = float(len(self._addrs) - 1) / 3
        self._messages = {}
        self._blocks = {}
        self._accepted_blocks = {}
        self._accepted_block_count = 0
        self._message_count = 0
        self._block_count = 0
        self._reject_count = 0
        self._state = "PRE_PREPARE"

        self._msg_targets = [x for x in xrange(len(self._addrs)) if x != self._id]

        # pre-prepare
        if self._is_proposer:
            self._init_proposer()

    def __prepare(self):
        self._state = "PREPARE"
        increment_dict(self._messages, self._msg)
        increment_dict(self._messages, self._msg)
        self._message_count += 2
        self._print("Node %d moves PREPARE" % self._id)
        self._put_queue('P', [self._msg, hash_value(self._msg)], self._msg_targets)

    def __commit(self):
        self._state = "COMMIT"
        self._block = hash_value(self._msg + self._h[len(self._h) - 1])
        increment_dict(self._blocks, self._block)
        self._block_count += 1
        self._print("Node %d created the block: %s and moves COMMIT" % (self._id, self._block))
        self._put_queue('C', self._block, self._msg_targets)

    def __reject(self):
        self._state = "REJECT"
        self._reject_count += 1
        self._print("Node %d moves REJECT and waits other blocks" % self._id)
        self._put_queue('R', None, self._msg_targets)

    def __recover_reject(self):
        if len(self._h) <= self._round:
            for b in self._accepted_blocks:
                if self._accepted_blocks[b] >= (self._k + 1):
                    self._block = b
                    self._h.append(self._block)
                    self._print("Node %d recovers its reject, accept the block: %s and added into blockchain"
                                % (self._id, self._block), ignore_verbose=True)
                    if not self._is_proposer:
                        self._put_queue('NR', "Next Round", [self._proposer_id])
                        self._round += 1
                        self._reset_round()
                    break
            if self._block is None and (self._reject_count + self._accepted_block_count) >= len(self._addrs):
                self._print("Node %d rejected the round!" % self._id, ignore_verbose=True)

    def _init_proposer(self):
        Node._init_proposer(self)
        self._messages[self._msg] = 1
        self._message_count += 1
        self._put_queue('PP', [self._msg, hash_value(self._msg)], self._msg_targets)

    # clear all previous stuffs
    def _reset_round(self):
        Node._reset_round(self)
        self._messages = {}
        self._blocks = {}
        self._accepted_blocks = {}
        self._accepted_block_count = 0
        self._message_count = 0
        self._block_count = 0
        self._reject_count = 0
        self._state = "PRE_PREPARE"

    def _recv(self):
        Node._recv(self)

        while True:
            payload = json.loads(self._socket_recv.recv())
            if payload['round'] == self._round:
                if payload['type'] == 'PP':
                    self._print("PP came to %d: %s (from %d)" % (self._id, payload['msg'][0], payload['id']))
                    if hash_check(payload['msg'][0], payload['msg'][1]):
                        self._msg = payload['msg'][0]
                        self.__prepare()
                        if self._messages[self._msg] >= (2 * self._k + 1):
                            self.__commit()
                        elif self._message_count == len(self._addrs):
                            self.__reject()
                    else:
                        self._state = "REJECT"
                        self._print("Hash value problem!")
                elif payload['type'] == 'P':
                    self._print("P came to %d: %s (from %d)" % (self._id, payload['msg'][0], payload['id']))
                    if hash_check(payload['msg'][0], payload['msg'][1]):
                        increment_dict(self._messages, payload['msg'][0])
                        self._message_count += 1
                        if self._msg is not None and self._state != "COMMIT" and \
                                self._messages[self._msg] >= (2 * self._k + 1):
                            self.__commit()
                        elif self._message_count == len(self._addrs) and self._state == "PREPARE":
                            self.__reject()
                    else:
                        self._state = "REJECT"
                        self._print("Hash value problem!")
                elif payload['type'] == 'C':
                    self._print("C came to %d: %s (from %d)" % (self._id, payload['msg'], payload['id']))
                    block = payload['msg']
                    increment_dict(self._blocks, block)
                    self._block_count += 1
                    if self._block is not None and self._blocks[self._block] >= (2 * self._k + 1):
                        # accept own block as a new block in blockchain
                        self._h.append(self._block)
                        self._print("Node %d accept the block: %s and added into blockchain" % (self._id, self._block),
                                    ignore_verbose=True)
                        if not self._is_proposer:
                            self._put_queue('NB', self._block, self._msg_targets)
                            self._put_queue('NR', "Next Round", [self._proposer_id])
                            self._round += 1
                            self._reset_round()
                        if self._is_proposer:
                            self._put_queue('NB', self._block, self._msg_targets)
                            self._next_round_count += 1
                            if self._next_round_count >= len(self._addrs):
                                self._round += 1
                                self._reset_round()
                                self._init_proposer()
                            else:
                                self._reset_round()
                elif payload['type'] == 'R':
                    self._reject_count += 1
                    if self._reject_count + self._accepted_block_count == len(self._addrs):
                        self.__recover_reject()
                elif payload['type'] == 'NB':
                    self._print("NB came to %d: %s (from %d)" % (self._id, payload['msg'], payload['id']))
                    block = payload['msg']
                    increment_dict(self._accepted_blocks, block)
                    self._accepted_block_count += 1
                    self.__recover_reject()
                elif payload['type'] == 'NR':
                    self._print("NR came to %d: %s (from %d)" % (self._id, payload['msg'], payload['id']))
                    self._next_round_count += 1
                    self._print(str(self._next_round_count))
                    if self._next_round_count >= len(self._addrs):
                        self._round += 1
                        self._reset_round()
                        self._init_proposer()


class Perfidious(Node):
    def __init__(self, **kwargs):
        Node.__init__(self, **kwargs)
        self._malicious_ids = kwargs.get('malicious_ids')
        self._honest_ids = kwargs.get('honest_ids')
        self._msg_prime = kwargs.get('msg_prime')
        self._h_prime = kwargs.get('h_prime')

        # Prepare groups
        self._first_group, self._second_group = [], []
        if self._proposer_id in self._malicious_ids:
            for index in range(len(self._honest_ids)):
                if index % 2 == 0:
                    self._first_group.append(self._honest_ids[index])
                else:
                    self._second_group.append(self._honest_ids[index])
            self._malicious_ids_except_self = [x for x in self._malicious_ids if x != self._id]
        else:
            self._first_group.append(self._proposer_id)
            self._honest_ids.remove(self._proposer_id)
            for index in range(len(self._honest_ids)):
                if index % 2 == 0:
                    self._second_group.append(self._honest_ids[index])
                else:
                    self._first_group.append(self._honest_ids[index])

        if self._is_proposer:
            self._init_proposer()

    def _init_proposer(self):
        Node._init_proposer(self)
        self._block = hash_value(self._msg + self._h[len(self._h) - 1])
        self._block_prime = hash_value(self._msg_prime + self._h_prime[len(self._h_prime) - 1])
        self._put_queue('PP', [self._msg, hash_value(self._msg)], self._first_group)
        self._put_queue('PP', [self._msg_prime, hash_value(self._msg_prime)], self._second_group)
        self._put_queue('PP', [self._msg, hash_value(self._msg)], self._malicious_ids_except_self)
        self._put_queue('C', self._block, self._first_group)
        self._put_queue('C', self._block_prime, self._second_group)
        self._put_queue('NB', self._block, self._first_group)
        self._put_queue('NB', self._block_prime, self._second_group)

    def _reset_round(self):
        self._h_prime.append(self._block_prime)
        self._h.append(self._block)
        self._round += 1
        Node._reset_round(self)

    def _recv(self):
        Node._recv(self)
        while True:
            payload = json.loads(self._socket_recv.recv())
            if payload['type'] == 'PP':
                if hash_check(payload['msg'][0], payload['msg'][1]):
                    self._msg = payload['msg'][0]
                    self._block = hash_value(self._msg + self._h[len(self._h) - 1])
                    self._block_prime = hash_value(self._msg_prime + self._h_prime[len(self._h_prime) - 1])
                    self._put_queue('P', [self._msg, hash_value(self._msg)], self._first_group)
                    self._put_queue('P', [self._msg_prime, hash_value(self._msg_prime)], self._second_group)
                    self._put_queue('C', self._block, self._first_group)
                    self._put_queue('C', self._block_prime, self._second_group)
                    self._put_queue('NB', self._block, self._first_group)
                    self._put_queue('NB', self._block_prime, self._second_group)
                    self._put_queue('NR', "Next Round", [self._proposer_id])
                    self._reset_round()
            elif payload['type'] == 'NR':
                self._next_round_count += 1
                if self._next_round_count >= (len(self._addrs) - 1):
                    self._reset_round()
                    self._init_proposer()


def create_process(proc_id, is_proposer, proposer_id, is_honest, ip_addrs, honest_ids, malicious_ids, io_lock, verbose):
    try:
        if is_honest[proc_id]:
            proc = Honest(id=proc_id, addrs=ip_addrs, is_proposer=is_proposer, proposer_id=proposer_id,
                          h=[''], io_lock=io_lock, verbose=verbose)
        else:
            proc = Perfidious(id=proc_id, addrs=ip_addrs, is_proposer=is_proposer, proposer_id=proposer_id,
                              h=[''], io_lock=io_lock, verbose=verbose,
                              honest_ids=honest_ids, malicious_ids=malicious_ids, msg_prime='fsociety', h_prime=[''])
        proc.start()
        proc.join()
    except KeyboardInterrupt:
        raise Exception('Process %s cannot be created...' % proc_id)


if __name__ == '__main__':
    freeze_support()

    _intro = '%(prog)s creates a distributed systems that resolve byzantine fault tolerance.'
    _help_process_count = 'Number of processes in distributed systems.'
    _help_traitor_count = 'Number of traitors in distributed systems.'
    _help_honest_proposer = 'Proposer will be honest if this flag is given.'
    _help_verbose = "Prints all operations"

    cmd_parser = argparse.ArgumentParser(description=_intro)
    cmd_parser.add_argument('-p', '--process_count', metavar='N', help=_help_process_count)
    cmd_parser.add_argument('-t', '--traitor_count', metavar='N', help=_help_traitor_count)
    cmd_parser.add_argument('--honest_proposer', action='store_true', default=False, help=_help_honest_proposer)
    cmd_parser.add_argument('--verbose', action='store_true', default=False, help=_help_verbose)
    cmd_args = cmd_parser.parse_args()

    """
        1) process_count: number of process (n = 3k + 1) -> k is tolerance
        2) traitor_count: number of traitors
        CONDITION: t should be smaller than n
    """
    _process_count = int(cmd_args.process_count)
    _traitor_count = int(cmd_args.traitor_count)

    # Global process variables
    _ip_addr_temp = 'tcp://127.0.0.1:%s'
    _ip_addrs = [_ip_addr_temp % str(x) for x in random.sample(xrange(5000, 30000), _process_count)]

    _malicious_ids = random.sample(xrange(_process_count), _traitor_count)
    _honest_ids = [x for x in xrange(_process_count) if x not in set(_malicious_ids)]
    _is_honest = [x not in set(_malicious_ids) for x in xrange(_process_count)]

    if cmd_args.honest_proposer or _traitor_count <= 0:
        _proposer_id = random.choice(_honest_ids)
    else:
        _proposer_id = random.choice(_malicious_ids)

    if _process_count >= _traitor_count and _process_count > 0:
        print "There are %d nodes in system. " \
              "%d of them is Honest (with ids %s). %d of them is Perfidious (with ids %s). Proposer is %d \n\n" \
              % (_process_count, _process_count - _traitor_count, str(_honest_ids),
                 _traitor_count, str(_malicious_ids), _proposer_id)
        processes = []
        _io_lock = Lock()
        for i in range(_process_count):
            processes.append(Process(target=create_process, args=(i, i == _proposer_id, _proposer_id, _is_honest,
                                                                  _ip_addrs, _honest_ids, _malicious_ids,
                                                                  _io_lock, cmd_args.verbose)))
            processes[i].start()
        try:
            [p.join() for p in processes]
        except KeyboardInterrupt:
            [p.terminate() for p in processes]
    else:
        raise IOError("Not suitable process or traitor count given")
